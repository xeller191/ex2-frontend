import Card from './card'

export default function Report() {
  return (
    <>
      <Card title="สวัสดีปัใหม่ 2024" detail="Happy new year 2024" />
      <Card title="สวัสดีปัใหม่ 2024" detail="Happy new year 2023" />
      <Card title="สวัสดีปัใหม่ 2024" detail="Happy new year 2023" />
      <Card title="สวัสดีปัใหม่ 2024" detail="Happy new year 2022" />
      <Card title="สวัสดีปัใหม่ 2024" detail="Happy new year 2022" />
      <Card title="สวัสดีปัใหม่ 2024" detail="Happy new year 2021" />
      <Card title="สวัสดีปัใหม่ 2024" detail="Happy new year 2021" />
      <Card title="สวัสดีปัใหม่ 2024" detail="Happy new year 2020" />
      <Card title="สวัสดีปัใหม่ 2024" detail="Happy new year 2019" />
    </>
  )
}
