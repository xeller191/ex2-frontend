import { AccountCircle, ThumbUpOffAlt } from '@mui/icons-material'
import { Box } from '@mui/material'

type Props = {
  title: string
  detail: string
}

export default function Card({ title, detail }: Readonly<Props>) {
  return (
    <div className="flex flex-row items-center justify-center my-1">
      <div className="w-4 h-4 rounded-full bg-gray-950" />
      <Box
        sx={{
          boxShadow: 1,
          borderRadius: '5px',
          width: '200px',
          height: '40px',
          display: 'flex',
          marginLeft: '5px',
          flexDirection: 'row',
          justifyContent: 'start',
          alignItems: 'center'
        }}
      >
        <AccountCircle />
        <div className="flex flex-col">
          <h1 className="text-xs ml-2">{title}</h1>
          <h1 className="text-[10px] ml-2">{detail}</h1>
        </div>
      </Box>
      <Box
        sx={{
          boxShadow: 1,
          borderRadius: '5px',
          width: '120px',
          height: '40px',
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        <ThumbUpOffAlt fontSize="small" className="text-blue-700" />
        <h1 className="text-xs ml-2">test test</h1>
      </Box>
    </div>
  )
}
