import { createSlice, PayloadAction } from '@reduxjs/toolkit'

interface StateType {
  path: string
  indexMenuSelected: number
}

const initialState: StateType = {
  path: '',
  indexMenuSelected: 0
}

const routeSlice = createSlice({
  name: 'routeSlice',
  initialState,
  reducers: {
    setRoute: (state, action: PayloadAction<StateType>) => {
      state.path = action.payload.path
      state.indexMenuSelected = action.payload.indexMenuSelected
    }
  }
})

export const { setRoute } = routeSlice.actions
export default routeSlice.reducer
