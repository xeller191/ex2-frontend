import { configureStore } from '@reduxjs/toolkit'

import routeReducer from './features/routeSlice'
export function makeStore() {
  return configureStore({
    reducer: {
      routeStore: routeReducer
    },
    devTools: true
  })
}

export const store = makeStore()

// Infer the type of makeStore
export type AppStore = ReturnType<typeof makeStore>
// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<AppStore['getState']>
export type AppDispatch = AppStore['dispatch']
