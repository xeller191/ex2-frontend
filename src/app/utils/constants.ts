export const LIST_MENU: TypeListMenu[] = [
  'Check',
  'Approval',
  'Report',
  'Statistics',
  'Settings'
]

export type TypeListMenu =
  | 'Check'
  | 'Approval'
  | 'Report'
  | 'Statistics'
  | 'Settings'

export const DRAWER_WITH = 240
