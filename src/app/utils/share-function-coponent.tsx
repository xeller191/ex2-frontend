import {
  Approval,
  CheckBox,
  Report,
  Settings,
  TravelExplore
} from '@mui/icons-material'
import { TypeListMenu } from './constants'
export const getIconWithListMenu = (listMenu: TypeListMenu | string) => {
  switch (listMenu) {
    case 'Check':
      return <CheckBox />

    case 'Settings':
      return <Settings />

    case 'Approval':
      return <Approval />

    case 'Report':
      return <Report />

    case 'Statistics':
      return <TravelExplore />
    default:
      return <></>
  }
}
