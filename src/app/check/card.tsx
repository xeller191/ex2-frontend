import { Box } from '@mui/material'

type Props = {
  text: string
  number: number
  Icon: React.ReactElement
}

export default function Card({ text, number, Icon }: Readonly<Props>) {
  return (
    <Box
      sx={{
        boxShadow: 3,
        width: '150px',
        padding: 5,
        margin: '10px',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
      }}
    >
      <div className="flex flex-row text-blue-600">
        {Icon}
        <h1 className="ml-2">{text}</h1>
      </div>
      <div className="mt-4 flex flex-col justify-center items-center">
        <h1 className="text-4xl font-bold">{number}</h1>
        <h1 className="text-xs">{text}</h1>
      </div>
    </Box>
  )
}
