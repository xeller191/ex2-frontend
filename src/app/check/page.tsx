'use client'
import {
  CardGiftcard,
  ChatBubbleOutline,
  ContentPaste,
  FavoriteBorder,
  ThumbUpOffAlt
} from '@mui/icons-material'
import Card from './card'
export default function Check() {
  return (
    <div className="flex flex-col items-center justify-center">
      <div className="w-[340px] flex flex-row justify-between">
        <h1 className="font-bold text-xl">In Test 2009</h1>
        <ContentPaste />
      </div>
      <div className=" flex-wrap flex-row flex justify-center">
        <Card Icon={<ThumbUpOffAlt />} number={22} text="Like" />
        <Card Icon={<ChatBubbleOutline />} number={6} text="Comments" />
        <Card Icon={<FavoriteBorder />} number={33} text="Favorite" />
        <Card Icon={<CardGiftcard />} number={100} text="Clipbaord" />
      </div>
    </div>
  )
}
