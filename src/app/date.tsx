'use client'
import { Button } from '@mui/material'
import { addDays } from 'date-fns'
import { useState } from 'react'
import { DateRangePicker, Range } from 'react-date-range'

export default function Home() {
  const [state, setState] = useState<Range[]>([
    {
      startDate: new Date(),
      endDate: addDays(new Date(), 7),
      key: 'selection'
    }
  ])
  return (
    <div>
      <Button>Click ME</Button>
      <DateRangePicker
        onChange={(item) => {
          return setState([item.selection])
        }}
        showDateDisplay={true}
        moveRangeOnFirstSelection={false}
        months={2}
        ranges={state}
        direction="horizontal"
      />
    </div>
  )
}
