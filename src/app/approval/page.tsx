'use client'
import { LineChart } from '../components/line-chart'

export default function Approval() {
  return (
    <div>
      <LineChart />
    </div>
  )
}
