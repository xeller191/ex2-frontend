import { DRAWER_WITH } from '@/app/utils/constants'
import { Box, Drawer } from '@mui/material'
import DrawerComponent from './drawer'

type Props = {
  mobileOpen: boolean
  handleDrawerToggle: () => void
}

export default function SidebarComponent({
  mobileOpen,
  handleDrawerToggle
}: Readonly<Props>) {
  return (
    <Box
      component="nav"
      sx={{ width: { sm: DRAWER_WITH }, flexShrink: { sm: 0 } }}
      aria-label="mailbox folders"
    >
      <Drawer
        variant="temporary"
        open={mobileOpen}
        onClose={handleDrawerToggle}
        ModalProps={{
          keepMounted: true // Better open performance on mobile.
        }}
        sx={{
          display: { xs: 'block', sm: 'none' },
          '& .MuiDrawer-paper': {
            boxSizing: 'border-box',
            width: DRAWER_WITH
          }
        }}
      >
        <DrawerComponent />
      </Drawer>
      <Drawer
        variant="permanent"
        sx={{
          display: { xs: 'none', sm: 'block' },
          '& .MuiDrawer-paper': {
            boxSizing: 'border-box',
            width: DRAWER_WITH
          }
        }}
        open
      >
        <DrawerComponent />
      </Drawer>
    </Box>
  )
}
