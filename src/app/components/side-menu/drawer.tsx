import {
  Divider,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Toolbar
} from '@mui/material'

import { setRoute } from '@/app/lib/features/routeSlice'
import { LIST_MENU, TypeListMenu } from '@/app/utils/constants'
import { getIconWithListMenu } from '@/app/utils/share-function-coponent'
import { useRouter } from 'next/navigation'
import { useDispatch } from 'react-redux'
export default function DrawerComponent() {
  const dispatch = useDispatch()
  const router = useRouter()
  return (
    <>
      <Toolbar />
      <Divider />
      <List>
        {LIST_MENU.map((menu: TypeListMenu, index: number) => {
          return (
            <ListItem
              key={menu}
              disablePadding
              onClick={() => {
                const path = `/${menu.toLowerCase()}`
                dispatch(setRoute({ indexMenuSelected: index, path }))
                return router.push(path)
              }}
            >
              <ListItemButton>
                <ListItemIcon>{getIconWithListMenu(menu)}</ListItemIcon>
                <ListItemText primary={menu} />
              </ListItemButton>
            </ListItem>
          )
        })}
      </List>
    </>
  )
}
