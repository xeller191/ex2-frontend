import Calendar from 'react-calendar'
import './App.css'

interface CalendarProps {
  setDate: any
  date: any
  selectRange: boolean
}
const CalendarComponent = (props: CalendarProps) => {
  const { setDate, date, selectRange } = props
  return <Calendar onChange={setDate} value={date} selectRange={selectRange} />
}

export default CalendarComponent
