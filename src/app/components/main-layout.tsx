'use client'

import { Box, CssBaseline, Toolbar } from '@mui/material'
import { useState } from 'react'
import BottomNavigationComponent from './bottom-navigation-component'
import CalendarComponent from './calendar/partials/calendar'
import Header from './header'
import SidebarComponent from './side-menu/sidebar'

export default function MainLayout({
  children
}: Readonly<{
  children: React.ReactNode
}>) {
  const [mobileOpen, setMobileOpen] = useState(false)
  const [date, setDate] = useState<any>(new Date())
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen)
  }
  return (
    <Box sx={{ display: 'flex', minWidth: '400px' }}>
      <CssBaseline />
      <Header handleDrawerToggle={handleDrawerToggle} />
      <SidebarComponent
        handleDrawerToggle={handleDrawerToggle}
        mobileOpen={mobileOpen}
      />
      <Box
        component="main"
        sx={{
          // width: { sm: `calc(100% - ${DRAWER_WITH}px)` },
          width: '100%',
          height: `calc(100vh - 64px)`,
          marginTop: '64px',
          display: 'flex',
          flexDirection: 'column'
        }}
      >
        <div className="h-[calc(100vh-64px)] flex justify-start flex-col items-center">
          <CalendarComponent setDate={setDate} date={date} selectRange={true} />
          <Toolbar />
          {children}
        </div>
        <BottomNavigationComponent />
      </Box>
    </Box>
  )
}
