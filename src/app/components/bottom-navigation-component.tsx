import { setRoute } from '@/app/lib/features/routeSlice'
import { BottomNavigation, BottomNavigationAction } from '@mui/material'
import { useRouter } from 'next/navigation'
import { useAppDispatch, useAppSelector } from '../lib/hook'
import { LIST_MENU, TypeListMenu } from '../utils/constants'
import { getIconWithListMenu } from '../utils/share-function-coponent'
export default function BottomNavigationComponent() {
  const dispatch = useAppDispatch()
  const { indexMenuSelected } = useAppSelector((state) => state.routeStore)
  const router = useRouter()
  return (
    <BottomNavigation
      sx={{ backgroundColor: 'transparent', display: { sm: 'none' } }}
      showLabels
      value={indexMenuSelected}
    >
      {LIST_MENU.map((menu: TypeListMenu, index: number) => (
        <BottomNavigationAction
          onClick={() => {
            const path = `/${menu.toLowerCase()}`
            dispatch(setRoute({ indexMenuSelected: index, path }))
            router.push(path)
          }}
          key={menu}
          label={menu}
          icon={getIconWithListMenu(menu)}
        />
      ))}
    </BottomNavigation>
  )
}
