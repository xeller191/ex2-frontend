declare global {
  declare module '@mui/material/styles' {
    interface Palette {
      myAwesomeColor: string
      textHeaderColor: string
      textContentColor: string
    }
    interface PaletteOptions {
      myAwesomeColor: string
      textHeaderColor: string
      textContentColor: string
    }
  }

  declare module '@mui/material/Button' {
    interface ButtonPropsColorOverrides {
      myAwesomeColor: true
      textHeaderColor: true
      textContentColor: true
    }
  }
}
