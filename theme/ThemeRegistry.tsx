'use client'

import CssBaseline from '@mui/material/CssBaseline'
import { ThemeProvider } from '@mui/material/styles'
import { theme } from './CreateTheme'
import { NextAppDirEmotionCacheProvider } from './EmotionCache'

//>>Add type
declare module '@mui/material/styles' {
  interface Palette {
    myAwesomeColor: string
    textHeaderColor: string
    textContentColor: string
  }
  interface PaletteOptions {
    myAwesomeColor: string
    textHeaderColor: string
    textContentColor: string
  }
}

declare module '@mui/material/Button' {
  interface ButtonPropsColorOverrides {
    myAwesomeColor: true
    textHeaderColor: true
    textContentColor: true
  }
}

export default function ThemeRegistry({
  children
}: Readonly<{
  children: React.ReactNode
}>) {
  return (
    <NextAppDirEmotionCacheProvider options={{ key: 'mui' }}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        {children}
      </ThemeProvider>
    </NextAppDirEmotionCacheProvider>
  )
}
