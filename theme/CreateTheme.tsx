import { createTheme } from '@mui/material'
import { red } from '@mui/material/colors'

export const theme = createTheme({
  palette: {
    primary: {
      main: '#074e9f'
    },
    secondary: {
      main: '#19857b'
    },
    myAwesomeColor: '#F5FAFE',
    error: {
      main: red.A400
    },
    textHeaderColor: '#344054',
    textContentColor: '#667085'
  }
})
